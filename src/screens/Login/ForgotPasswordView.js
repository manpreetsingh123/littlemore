import React from 'react';
import { View, Text } from 'react-native';
import ForgotPasswordForm from '~/components/Login/ForgotPasswordForm';

const ForgotPasswordView = () => {
    return <View>
        <ForgotPasswordForm />
    </View>
}

export default ForgotPasswordView;