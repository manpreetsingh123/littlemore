import Constants from 'expo-constants';
import { Permissions, FaceDetector, DangerZone } from 'expo';
import React,{useState,useEffect} from 'react';
import { StyleSheet, Text, View, Platform, Button } from 'react-native';
import { Camera } from 'expo-camera';

export default function LoginForm() {
    const [hasPermission, setHasPermission] = useState('');
    const [face,setFace] = useState([]);

    const [faces, setFaces] = useState(null)
    const detectFaces = async faces => {
      if(faces.faces<1)
      return;
    setFaces(faces.faces);
    setFaceDetected(true)
    };
    const detectLandmarks = async imageUri => {
      const options = { mode: FaceDetector.Constants.Landmarks.none };
      return await FaceDetector.detectFacesAsync(imageUri, options);
    };
    const runClassifications = async imageUri => {
      const options = { mode: FaceDetector.Constants.Classifications.none};
      return await FaceDetector.detectFacesAsync(imageUri, options);
    };
    const faceDetected = ({faces}) => {
      setFaces({faces})
      console.log({faces})
      if(face.face<1)
      return;
    setFaces(face.face);
    }
  
    useEffect(() => {
      (async () => {
        const { status } = await Camera.requestPermissionsAsync();
        setHasPermission(status === 'granted');
      })();
    }, []);
  
    if (hasPermission !== true) {
      return <Text>No access to camera</Text>
    }
  
    return (
      //<View style={{ flex: 1 }}>
        <Camera
          style={{ flex: 1 }}
          type='front'
          onFacesDetected = {faceDetected}
          faceDetectorSettings={{
        
        tracking: true,
        minDetectionInterval:300
      }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
            <Text style= {{top:10}}>Hi Aman  </Text>
          </View>
        </Camera>
      //</View>
    );
  }
  